package br.com.itau.marketplace.services;
import br.com.itau.marketplace.models.*;
import br.com.itau.marketplace.repositories.ClienteRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClienteService {
	@Autowired
	ClienteRepository clienteRepository;
	
	public Iterable<Cliente> obterCliente() {
//	List<Cliente> Clientes = new ArrayList<Cliente>();
//		
//		Cliente cliente = new Cliente();
//		cliente.setId(0);
//		cliente.setNome("teste");
//		cliente.setEmail("ayrtonhe");
//		
//		Clientes.add(cliente);
//		return Clientes;
		
		return clienteRepository.findAll();
		
	}
	
	public Optional<Cliente> buscarClientePorId(int id){
		return clienteRepository.findById(id);
	}
	public void inserirCliente(Cliente cliente) {
		clienteRepository.save(cliente);
	}
}
