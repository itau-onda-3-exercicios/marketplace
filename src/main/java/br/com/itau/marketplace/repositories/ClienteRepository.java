package br.com.itau.marketplace.repositories;

import org.springframework.data.repository.CrudRepository;

import br.com.itau.marketplace.models.Cliente;

public interface ClienteRepository extends CrudRepository<Cliente, Integer>{

}
