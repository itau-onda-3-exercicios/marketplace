package br.com.itau.marketplace.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.marketplace.models.Cliente;
import br.com.itau.marketplace.services.ClienteService;

@RestController
@RequestMapping("/cliente")
public class ClienteController {
	@Autowired
	ClienteService ClienteService;
	
	@GetMapping
	public Iterable<Cliente> listar() {
		return ClienteService.obterCliente();
	}
	
	@GetMapping("/tchau")
	public String mostrarTchau() {
		return "tchau mundo";
	}
	
	@PostMapping
	public void mostrarTchau(@RequestBody Cliente cliente) {
		ClienteService.inserirCliente(cliente);
	}
	
	@GetMapping("/{id}")
	public Optional<Cliente> obterClientePorId(@PathVariable int id) {
		return ClienteService.buscarClientePorId(id);
	}
}
